import React from 'react';
import { StyleSheet, Text, TextInput, View , TouchableOpacity} from 'react-native';
import { FlatList } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
 

class AutoComplete extends React.Component {
 
suggestionSelected (details) {
    console.log("suggestionSelected", details.formatted_address, details.geometry.location.lat, details.geometry.location.lng);
            const lat = details.geometry.location.lat;
            const lon = details.geometry.location.lng; 
            const choosenLocation = details.formatted_address;

    fetchDetails = false
    this.props.getLocationByName(choosenLocation, lat, lon)
    
}


render () {
        return(
            <View style={styles.container}>
      
                    <GooglePlacesAutocomplete
                            placeholder='Search'
                            minLength={1} // minimum length of text to search
                            autoFocus={true}
                            returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                            listViewDisplayed={false}  // true/false/undefined
                            fetchDetails={true}
                            // renderDescription={row => row.description} // custom description render
                            onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                this.suggestionSelected(details, details.formatted_address, details.geometry.location.lat, details.geometry.location.lng)
                                return(null)
                               
                                    }}
                                    
                            getDefaultValue={() => ''}
                            
                            query={{
                                        // available options: https://developers.google.com/places/web-service/autocomplete
                                        key: 'AIzaSyBg18Aa8Q1ybtsP0m3ZZYkhGi-onQD3dx0',
                                        language: 'en', // language of the results
                                        types: '(cities)' // default: 'geocode'
                                    }}
                                    
                            styles={{
                                container: {
                                    marginTop: 0,
                                    borderBottomWidth: 2,
                                },
                                textInputContainer: {
                                    backgroundColor: 'rgba(0,0,0,0)',
                                    borderTopWidth: 0,
                                    borderBottomWidth:0
                                },
                                textInput: {
                                    marginLeft: 10,
                                    marginRight: 0,
                                    marginTop: 0, 
                                    height: 38,
                                    color: '#5d5d5d',
                                    fontSize: 16
                                },
                                predefinedPlacesDescription: {
                                    color: '#1faadb'
                                },
                                }}
                                    
                            currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
                            currentLocationLabel="Current location"
                            // nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                            // GoogleReverseGeocodingQuery={{
                            //      // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                            // }}
                            // GooglePlacesSearchQuery={{
                            //     // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                            //     rankby: 'distance',
                            //     types: 'food'
                            // }}
                                
                                    // filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                                    // predefinedPlaces={[homePlace, workPlace]}
                                
                                    debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                                    // renderLeftButton={()  => <Image source={require('path/custom/left-icon')} />}
                                    // renderRightButton={() => <Text>Custom text after the input</Text>}
                                    />
                                
                                
                     
                        {/* <TouchableOpacity style={styles.button} onPress={(text) => this.buttonClicked(text)}>
                                <Text style={styles.buttonText} >Search</Text>
                                {/* <Image
                                    style={styles.button}
                                    source={require('./myButton.png')}
                                /> */}
                        {/* </TouchableOpacity>  */}
                      
    
        
               

            </View>
        )
    }
}
const styles = StyleSheet.create({

    container: {
    //  flex: 1,
     paddingTop: 5,
     paddingBottom: 0,
     flexDirection: "row",

    }
  })

  
export default AutoComplete



//API KEY Google : AIzaSyBg18Aa8Q1ybtsP0m3ZZYkhGi-onQD3dx0

// findSuggestions () {
//     // Construct the API url to call
//     // let url = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBg18Aa8Q1ybtsP0m3ZZYkhGi-onQD3dx0&libraries=places";
//     let url = "https://maps.googleapis.com/maps/api/place/autocomplete/json&input="+this.state.text+"&key=AIzaSyBg18Aa8Q1ybtsP0m3ZZYkhGi-onQD3dx0&libraries=places"
//     // Call the API, and set the state of the weather forecast
//     fetch(url)
//     .then(response => response.json())
//     .then(response => {
//         console.log(response)
//         // this.setState(() => ({
//         // 	suggestions: data
//         // }));
//     })
// }
   /* <TextInput
                            id={AutoComplete}
                            value={this.state.text} 
                            onChangeText={(text) => this.changeInput(text)}
                            style={styles.input}
                        >
                        </TextInput> */


                        // changeInput = (text) => {
                        //     // console.log(text)
                        //         this.setState({
                        //         text : text
                        //         });
                        
                        //     let value = this.state.text 
                        
                        
                        //     // let suggestions = [];
                        
                        //     // let url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input='+this.state.text+'&key=AIzaSyBg18Aa8Q1ybtsP0m3ZZYkhGi-onQD3dx0&libraries=places'
                        //     // fetch(url)
                        //     // .then(response => response.json())
                        //     // .then(response => {
                        //     //     console.log(response.predictions)
                        //     //     const newItems =[]
                        //     //     for (var i=0; i<response.predictions.length; i++){
                        //     //         console.log(response.predictions[i])
                        //     //         newItem = response.predictions[i].description
                        //     //         id = response.predictions[i].id
                        //     //         placeID = response.predictions[i].place_id
                           
                        //     //         objectItem ={
                        //     //             name : newItem,
                        //     //             id : id, 
                        //     //             placeID : placeID
                        //     //         }
                        //     //         newItems.push(objectItem)
                        //     //         // console.log(newItems)
                        //     //     }
                        
                        //     //     this.setState(() => ({
                        //     //     	items: newItems
                        //     //     }));
                        //     // })
                        //     // .catch( error => console.log(error))
                            
                            
                        //     // if (value.length > 0) {
                        //     //     const regex = new RegExp (`^${value}`, 'i');
                        //     //     suggestions = this.state.items.sort().filter(v => regex.test(v));
                        //     //     console.log("filter suggestions", suggestions)
                        //     // }
                        //     // this.setState(()=> ({
                        //     //     suggestions : suggestions
                        //     // }))
                        
                        // }


                        // renderSuggestions () {
                        //     const { items } = this.state
                        //     // console.log(this.state.items)
                        //     if (items.length === 0){
                        //         return null;
                        //     } else {
                        //         return (
                        //             <View>
                        //                 <FlatList
                        //                     data ={items}
                        //                     renderItem={({ item })=> <Text 
                        //                                     title={item.name}
                        //                                     onPress={() => this.suggestionSelected(item)} 
                        //                                     style={styles.item}>
                        //                                     {item.name}</Text>}
                        //                     keyExtractor={item => item.id}                    
                        //                 />
                                        
                        //             </View>
                        //         )
                        //     // }
                        //     // const { suggestions } = this.state;
                        //     // console.log(this.state.suggestions)
                        //     // if (suggestions.length === 0) {
                        //     //     return null;
                        //     // }
                        //     // else { 
                        //     // return (
                        //     //     <View>  
                        //     //         <FlatList
                        //     //                 // data={[
                        //     //                 //     {key:'Emma'},
                        //     //                 //     {key:'Sam'},
                        //     //                 //     {key:'Maria'}
                        //     //                 // ]}
                        
                        //     //                 data ={[suggestions]}
                        //     //                 renderItem={({item})=> <Text 
                        //     //                                 onPress={(value) => this.suggestionSelected(value)} 
                        //     //                                 style={styles.item}>{item["key"]}</Text>}
                                                            
                        //     //             />
                        //     // </View>
                        //     // )
                        //     } }
                        
                        