import React from 'react';

import { StyleSheet, Text, View, ImageBackground} from 'react-native';
import FilterData from './FilterData.js'
// import { Constants, Location, Permissions } from 'expo';
// import BackgroundImage from './BackgroundImage'

export default  class Header extends React.Component {

constructor(props){
	super(props);
    this.state={
        datetime: 0,
         day:'Monday',
        };
    }


componentDidMount(){
//   console.log('Looking for date and time')
  fetch("http://worldtimeapi.org/api/ip") //works
      .then( res => res.json())
      .then( response =>  
        // console.log(response.day_of_week) &
        this.setState({
            datetime: response.datetime,
            day : response.day_of_week
        })
    )    
    .catch( error => console.log(error))
    }

    render(props) {   
       console.log('props', this.props)
      return (
        <View style={styles.container}>

            <ImageBackground  source={require('./Images/background_1.jpg')} style={{width: '100%', height: '100%'}}>
                <Text style={styles.textSmall}>Today</Text>
                <Text style={styles.inline}>
                  <Text >
                  
                    {this.state.day === 1 ? 
                          <Text style={styles.header}>Monday {" "}</Text>
                          : this.state.day === 2 ? 
                              <Text style={styles.header}>Tuesday {" "}</Text>
                                : this.state.day === 3 ? 
                                  <Text style={styles.header}>Wednesday {" "}</Text>
                                    : this.state.day === 4 ? 
                                      <Text style={styles.header}>Thursday {" "}</Text>
                                        : this.state.day === 5 ? 
                                          <Text style={styles.header}>Friday {" "}</Text>
                                            : this.state.day === 6 ? 
                                            <Text style={styles.header}>Saturday {" "} </Text>
                                              : this.state.day === 0 ? 
                                              <Text style={styles.header}>Sunday {" "}</Text>
                                                  :null }
                    {this.state.datetime.length > 0 ?  <FilterData  catchCurrentDate={this.props.catchCurrentDate.bind(this)} datetime={this.state.datetime} /> : null }
              </Text>
              </Text>
              </ImageBackground>
            </View>

        
      )
    }
}




  const styles = StyleSheet.create({
    with_background:{
      display: 'flex'
    },
    textSmall: {
      fontSize: 15,
      color: '#FFFFFF',
      textAlign: 'center',
      marginTop: 30, 
      fontFamily: 'sans-serif'
    }, 
    container: {
        // display: 'flex', 
        height: 120,
      
    },
    header: {
        color: '#FFFFFF',
        marginTop: 30,
        fontSize: 25,
        textAlign: 'center',
        paddingRight: 20, 
        fontFamily: "sans-serif-medium"
    },

    // image: {
    //   width: 32, 
    //   height: 40,
    //   marginLeft: 30, 
    //   marginTop: 40,
    // },
    inline:{
      textAlign: 'center',
    }
    
  })