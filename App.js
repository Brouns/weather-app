import React from 'react';
import { FlatList, Button } from 'react-native';
import ForecastCard from './ForecastCard';
import Header from './Header'; 
import AutoComplete from './AutoComplete'
import { StyleSheet, Text, View, Image, ImageBackground } from 'react-native';
import { removeOrientationChangeListener } from 'expo/build/ScreenOrientation/ScreenOrientation';


export default class App extends React.Component {

	constructor(props){
		super(props);
		
		this.state = {
			latitude: 0,
			longitude: 0,
			forecast: [],
			error:''
		};
	}

	componentDidMount(){
		// Get the user's location
		this.getLocation();
	}

	getLocation(){
		// Get the current position of the user
		navigator.geolocation.getCurrentPosition(
			(position) => {
				this.setState(
					(prevState) => ({
					latitude: position.coords.latitude, 
					longitude: position.coords.longitude
					}), () => { this.getWeather(); }
				);
			},
			(error) => this.setState({ forecast: error.message }),
			{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
		);

	}

	getLocationByName = (choosenLocation, lat, lon) => {
		console.log(choosenLocation, lat, lon)
		this.setState({
			latitude : lat,
			longitude : lon
		})
		this.getWeather()
	}


	getWeather(){

		// Construct the API url to call
		let url = 'https://api.openweathermap.org/data/2.5/forecast?lat=' + this.state.latitude + '&lon=' + this.state.longitude + '&units=metric&appid=16909a97489bed275d13dbdea4e01f59';

		// Call the API, and set the state of the weather forecast
		fetch(url)
		.then(response => response.json())
		.then(data => {
			this.setState((prevState, props) => ({
				forecast: data
			}))
			
		})
		.catch( error => console.log(error));
	}

	catchCurrentDate(dateSplit, monthSplit, yearSplit) {
		console.log('catchCurrentDate', dateSplit, monthSplit, yearSplit)
		this.setState({
			currentDay: dateSplit,
			currentMonth: monthSplit,
			currentYear: yearSplit
		})
	}
	
	render() {		
		return (
      <View >

				
							<Header catchCurrentDate={this.catchCurrentDate.bind(this)}/> 
							<AutoComplete getLocationByName ={this.getLocationByName.bind(this)}/> 
							
							<FlatList data={this.state.forecast.list} 
												style={styles.container}
												keyExtractor={item => item.dt_txt} 
												renderItem={({item}) => <ForecastCard 
																											currentDay={this.state.currentDay}
																											detail={item} 
																											location={this.state.forecast.city.name} 
																											/>} />
			
      
      </View>
		);
	}
}
const styles = StyleSheet.create({
    container: {
      marginTop: 0,
			marginLeft: 10,
    }
})








// import React from 'react';
// import { StyleSheet, Text, View, Image } from 'react-native';
// // import { Card, Divider } from 'react-native-elements';
// import { FlatList } from 'react-native';
// import ForecastCard from './ForecastCard'

// class App extends React.Component {


//   constructor(props) {
//     super(props);
//     this.state={
//       latitide: 0,
//       longitude:0,
//       forecast:[],
//       error:''
//     }
//   }

//   getLocation (){
//     navigator.geolocation.getCurrentPosition(
//       (position) => {
//           this.setState(
//           (prevState) => ({
//               latitude: position.coords.latitude, 
//               longitude: position.coords.longitude
//               }), () => { this.getWeather(); }
//           );
//       },
//       (error) => this.setState({ forecast: error.message }),
//         { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
//     );
//   }


//   getWeather(){

//     // Construct the API url to call
//     let url = 'https://api.openweathermap.org/data/2.5/forecast?lat=' + this.state.latitude + '&lon=' + this.state.longitude + '&units=metric&appid=16909a97489bed275d13dbdea4e01f59';

//     // Call the API, and set the state of the weather forecast
//     fetch(url)
//     console.log(url)
//     .then(response => response.json())
//     console.log(response)
//     .then(data => {
//         this.setState((prevState, props) => ({
//             forecast: data
//     }));
//     })
// }

//   render () {
//     console.log("in App")
//   return (
//     <View style={styles.container}>
//       <Text>The weather app ON Ruben's phone</Text>
//       <Button onPress={this.getLocation()}></Button>
//       <FlatList 
//           data={this.state.forecast.list} 
//           style={{marginTop:20}} 
//           keyExtractor={item => item.dt_text} 
//           renderItem={({item}) => 
//               <ForecastCard 
//                   detail={item} 
//                   location={this.state.forecast.city.name} 
//                   />} 
//               />
//     </View>
//   );
//   }
// }
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });

// export default App


	// console.log('get location by name', choosenLocation.description )
		// placeID = choosenLocation.place_id
		// console.log(placeID)
		// let url = 'https://api.openweathermap.org/data/2.5/forecast?id='+placeID+'&units=metric&appid=16909a97489bed275d13dbdea4e01f59'
		// fetch(url)
		// .then(response => response.json())
		// .then(response => 
		// 	console.log(response))
		
		// 	.catch( error => console.log(error))


			// let url = 'https://maps.googleapis.com/maps/api/geocode/json?place_id='+placeID+'&key=AIzaSyBg18Aa8Q1ybtsP0m3ZZYkhGi-onQD3dx0&libraries=places'
			// // https://maps.googleapis.com/maps/api/geocode/json?place_id=ChIJOwE7_GTtwokRFq0uOwLSE9g&key=KEY_GOES_HERE
			// fetch(url)
			// .then(response => response.json())
			// .then(response => {
			// 		console.log('output from getLocationByName', response)
			// 		})