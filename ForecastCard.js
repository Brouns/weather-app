
import React, {Component} from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Text, Card, Divider } from 'react-native-elements';


export default class ForecastCard extends Component {
    state={

    }
    componentDidMount () {
        let currentDay = this.props.currentDay
        if (currentDay === '01' || currentDay === '02' || currentDay === '03' || currentDay === '04' || currentDay ==='05' || currentDay ==='06' || currentDay ==='07' || currentDay ==='08'|| currentDay ==='09') 
             {
                // console.log('current days is a string')
                const currentDayOneDigit = parseInt(currentDay)
                // console.log('currentdayOneDigit', currentDayOneDigit)
                this.setState({
                    currentDayOneDigit
                })
        } else { 
                const currentDayOneDigit = parseInt(currentDay)
                // console.log('currentdayOneDigit', currentDayOneDigit) 
                this.setState({
                    currentDayOneDigit
                })
        }
            let time;
            // Create a new date from the passed date time
            var date = new Date(this.props.detail.dt*1000);
            // Hours part from the timestamp
            var hours = date.getHours();
            var date2OneDigit = date.getDate();
            var date2 = ('0' + date2OneDigit).slice(-2)
            // console.log('date2', date2OneDigit)
            var year =date.getFullYear();
            // console.log('year', year)
            var month = date.getMonth() + 1; 
            // console.log('month', month)
            // Minutes part from the timestamp
            var minutes = "0" + date.getMinutes();

            time = hours + ':' + minutes.substr(-2);
            // console.log(this.props.detail)
            this.setState({
                currentDay,
                date2,
                date2OneDigit,
                month,
                time,
            })
        }

	render(props) {
        console.log(this.state.date2OneDigit, this.state.currentDayOneDigit)
        if (this.state.date2OneDigit === this.state.currentDayOneDigit) {
           
		return (
            
			<View style={styles.card}>
                <View style={styles.innerCard}>
                        <Text style={styles.place}>{this.props.location}</Text>
                        <Text style={styles.place}>{this.state.date2} - {this.state.month}</Text>
                        <Text style={styles.time}>{this.state.time}</Text>
                </View>
                
				<Divider style={{ backgroundColor: '#dfe6e9', marginVertical:5}} />
				
				<View style={styles.innerCardGrid}>
                    <Text style={styles.notes}>{this.props.detail.weather[0].description}</Text>
					<Image style={styles.image} source={{uri:"https://openweathermap.org/img/w/" + this.props.detail.weather[0].icon + ".png"}} />
					{Math.round( this.props.detail.main.temp * 10) / 10 >= 15 ? 
                        <Text style={styles.notesTempHot}>{Math.round( this.props.detail.main.temp * 10) / 10 }{" "}&#8451;</Text>
                        : <Text style={styles.notesTempCold}>{Math.round( this.props.detail.main.temp * 10) / 10 }{" "}&#8451;</Text>}
				</View>
			</View>
		 );
        }
        
        if (this.state.date2OneDigit === (this.state.currentDayOneDigit + 1) || this.state.currentDay === '01') {
            return (

			<View style={styles.card_otherDay01}>
                <View style={styles.innerCard}>
                        <Text style={styles.place}>{this.props.location}</Text>
                        <Text style={styles.place}>{this.state.date2} - {this.state.month}</Text>
                        <Text style={styles.time}>{this.state.time}</Text>
                </View>
                
                <Divider style={{ backgroundColor: '#dfe6e9', marginVertical:5}} />
				
				<View style={styles.innerCardGrid}>
                    <Text style={styles.notes}>{this.props.detail.weather[0].description}</Text>
					<Image style={styles.image} source={{uri:"https://openweathermap.org/img/w/" + this.props.detail.weather[0].icon + ".png"}} />
					{Math.round( this.props.detail.main.temp * 10) / 10 >= 15 ? 
                        <Text style={styles.notesTempHot}>{Math.round( this.props.detail.main.temp * 10) / 10 }{" "}&#8451;</Text>
                        : <Text style={styles.notesTempCold}>{Math.round( this.props.detail.main.temp * 10) / 10 }{" "}&#8451;</Text>}
				</View>

				
			</View>
		);
            
         }
        if (this.state.date2OneDigit === (this.state.currentDayOneDigit + 2) || this.state.currentDay === '02') {
            return (

			<View style={styles.card_otherDay02}>
                <View style={styles.innerCard}>
                        <Text style={styles.place}>{this.props.location}</Text>
                        <Text style={styles.place}>{this.state.date2} - {this.state.month}</Text>
                        <Text style={styles.time}>{this.state.time}</Text>
                </View>
                
                <Divider style={{ backgroundColor: '#dfe6e9', marginVertical:5}} />
				
				<View style={styles.innerCardGrid}>
                    <Text style={styles.notes}>{this.props.detail.weather[0].description}</Text>
					<Image style={styles.image} source={{uri:"https://openweathermap.org/img/w/" + this.props.detail.weather[0].icon + ".png"}} />
					{Math.round( this.props.detail.main.temp * 10) / 10 >= 15 ? 
                        <Text style={styles.notesTempHot}>{Math.round( this.props.detail.main.temp * 10) / 10 }{" "}&#8451;</Text>
                        : <Text style={styles.notesTempCold}>{Math.round( this.props.detail.main.temp * 10) / 10 }{" "}&#8451;</Text>}
				</View>

				
			</View>
		);
            
        }
        if (this.state.date2OneDigit  === (this.state.currentDayOneDigit + 3) || this.state.currentDay === '03') {
            return (

			<View style={styles.card_otherDay03}>
                <View style={styles.innerCard}>
                        <Text style={styles.place}>{this.props.location}</Text>
                        <Text style={styles.place}>{this.state.date2} - {this.state.month}</Text>
                        <Text style={styles.time}>{this.state.time}</Text>
                </View>
                
                <Divider style={{ backgroundColor: '#dfe6e9', marginVertical:5}} />
				
				<View style={styles.innerCardGrid}>
                    <Text style={styles.notes}>{this.props.detail.weather[0].description}</Text>
					<Image style={styles.image} source={{uri:"https://openweathermap.org/img/w/" + this.props.detail.weather[0].icon + ".png"}} />
					{Math.round( this.props.detail.main.temp * 10) / 10 >= 15 ? 
                        <Text style={styles.notesTempHot}>{Math.round( this.props.detail.main.temp * 10) / 10 }{" "}&#8451;</Text>
                        : <Text style={styles.notesTempCold}>{Math.round( this.props.detail.main.temp * 10) / 10 }{" "}&#8451;</Text>}
				</View>

				
			</View>
		);
            
        }
        if (this.state.date2OneDigit  === this.state.currentDayOneDigit + 4 || this.state.currentDay === '04') {
            return (

			<View style={styles.card_otherDay04}>
                <View style={styles.innerCard}>
                        <Text style={styles.place}>{this.props.location}</Text>
                        <Text style={styles.place}>{this.state.date2} - {this.state.month}</Text>
                        <Text style={styles.time}>{this.state.time}</Text>
                </View>
                
                <Divider style={{ backgroundColor: '#dfe6e9', marginVertical:5}} />
				
				<View style={styles.innerCardGrid}>
                    <Text style={styles.notes}>{this.props.detail.weather[0].description}</Text>
					<Image style={styles.image} source={{uri:"https://openweathermap.org/img/w/" + this.props.detail.weather[0].icon + ".png"}} />
					{Math.round( this.props.detail.main.temp * 10) / 10 >= 15 ? 
                        <Text style={styles.notesTempHot}>{Math.round( this.props.detail.main.temp * 10) / 10 }{" "}&#8451;</Text>
                        : <Text style={styles.notesTempCold}>{Math.round( this.props.detail.main.temp * 10) / 10 }{" "}&#8451;</Text>}
				</View>

				
			</View>
		);
            
        }
        if (this.state.date2OneDigit  === this.state.currentDayOneDigit + 5 || this.state.currentDay === '05') {
            return (

			<View style={styles.card_otherDay05}>
                <View style={styles.innerCard}>
                        <Text style={styles.place}>{this.props.location}</Text>
                        <Text style={styles.place}>{this.state.date2} - {this.state.month}</Text>
                        <Text style={styles.time}>{this.state.time}</Text>
                </View>
                
                <Divider style={{ backgroundColor: '#dfe6e9', marginVertical:5}} />
				
				<View style={styles.innerCardGrid}>
                    <Text style={styles.notes}>{this.props.detail.weather[0].description}</Text>
					<Image style={styles.image} source={{uri:"https://openweathermap.org/img/w/" + this.props.detail.weather[0].icon + ".png"}} />
					{Math.round( this.props.detail.main.temp * 10) / 10 >= 15 ? 
                        <Text style={styles.notesTempHot}>{Math.round( this.props.detail.main.temp * 10) / 10 }{" "}&#8451;</Text>
                        : <Text style={styles.notesTempCold}>{Math.round( this.props.detail.main.temp * 10) / 10 }{" "}&#8451;</Text>}
				</View>

				
			</View>
		);
} }
}

const styles = StyleSheet.create({
    innerCard: {
        flexDirection:'row', 
        justifyContent:'space-between', 
        // alignItems:'center',
        marginVertical:0,
    },
  
    innerCardGrid: {
        flexDirection:'row', 
        // display: 'flex',
        // gridTemplateColumns: '100 50 50', 
        justifyContent:'space-between', 
        // alignItems:'center',
        marginVertical:0,
    },
	card:{
		backgroundColor:'#A7D5DD',
        borderRadius:20,
        width: 350,
        height: 110,
        marginBottom: 10,
        // paddingTop: 10, 
        marginTop: 10, 
        marginRight: 10, 
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10, 
    },
    card_otherDay01:{
		backgroundColor:'#d7f4f4',
        borderRadius:20,
        width: 350,
        height: 110,
        marginBottom: 10,
        // paddingTop: 10, 
        marginTop: 10, 
        marginRight: 10, 
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10, 
    },
    card_otherDay02:{
		backgroundColor:'#dbebf0',
        borderRadius:20,
        width: 350,
        height: 110,
        marginBottom: 10,
        // paddingTop: 10, 
        marginTop: 10, 
        marginRight: 10, 
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10, 
    },
    card_otherDay03:{
		backgroundColor:'#b3ecff',
        borderRadius:20,
        width: 350,
        height: 110,
        marginBottom: 10,
        // paddingTop: 10, 
        marginTop: 10, 
        marginRight: 10, 
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10, 
    },
    card_otherDay04:{
		backgroundColor:'#e5e5ff',
        borderRadius:20,
        width: 350,
        height: 110,
        marginBottom: 10,
        // paddingTop: 10, 
        marginTop: 10, 
        marginRight: 10, 
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10, 
    },
    card_otherDay05:{
		backgroundColor:'#9cd2e2',
        borderRadius:20,
        width: 350,
        height: 110,
        marginBottom: 10,
        // paddingTop: 10, 
        marginTop: 10, 
        marginRight: 10, 
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10, 
    },
    image: {
        width:50, 
        height:50,
        paddingBottom: 0,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        marginTop: 0,

    },
	time:{
		fontSize:20,
        color:'#04013F',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 5,
        marginTop: 10,
        fontFamily: 'sans-serif-light'
	},
	notes: {
		fontSize: 18,
        color:'#04013F',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        marginTop: 5,
        fontFamily: 'sans-serif-light',
        width: 90
    },
    notesTempHot: {
		fontSize: 20,
        color:'#ff751a',
        fontWeight: "bold", 
        marginLeft: 10,
        marginRight: 0,
        marginBottom: 10,
        marginTop: 5,
        fontFamily: 'sans-serif-light',
        width: 70
    },
    notesTempCold: {
		fontSize: 20,
        color:'#0099cc',
        fontWeight: "bold", 
        marginLeft: 10,
        marginRight: 0,
        marginBottom: 10,
        marginTop: 5,
        fontFamily: 'sans-serif-light',
        width: 70
    },
    place: {
        fontSize: 20,
        color:'#04013F',
        marginTop: 10,
        marginLeft: 10,
        marginRight:10,
        marginBottom: 5, 
        fontFamily:'sans-serif-medium'
    }
}); 