import React, { Component } from 'react';
import { Image, AppRegistry } from 'react-native';

export default class BackgroundImage extends Component {

  render() {

    console.log("loading background image")
    const remote = 'https://jooinn.com/images/abstract-background-357.jpg';

    return (
       
      <Image
        style={{
            width: 500,
            height: 500,
            resizeMode: 'center',
        //     flex: 1,
        // //   resizeMode,
        }}
        source={require('./Images/background_1.jpg')}
      />
    );
  }
}

AppRegistry.registerComponent('BackgroundImage', () => BackgroundImage);
